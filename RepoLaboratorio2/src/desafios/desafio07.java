package desafios;

public class desafio07 {
    public static void main(String[] args) {
    	
    	//Trigonometricas
        double coseno = Math.cos(180);
        
        double seno = Math.sin(60);
        
        double tangente = Math.tan(75);
        
        double arcoTangente = Math.atan(50);
        
        double ordAbs = Math.atan2(8,9);
        

        double exponente = Math.exp(3);
        
        double logaritmo = Math.log(exponente);

        final double pi = Math.PI;
        final double e = Math.E;

        System.out.println("Funciones Trinogometricas: ");
        
        System.out.println("Coseno: "+coseno);
        System.out.println("Seno: "+seno);
        System.out.println("Tangente: "+tangente);
        System.out.println("Arcotangente: "+arcoTangente);
        System.out.println("Ordenada y Abscisa: "+ordAbs);

        System.out.println("Exponencial y la Inversa: ");
        
        System.out.println("Exponencial: "+exponente);
        System.out.println("Logaritmo = "+logaritmo);

        System.out.println("Constantes Pi y e:");
        
        System.out.println("pi: "+pi);
        System.out.println("e: "+e);

    }
}

