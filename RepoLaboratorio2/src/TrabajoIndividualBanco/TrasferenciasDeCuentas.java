package TrabajoIndividualBanco;

class TrasferenciasDeCuentas {
	
	public static void Transferencias(CuentaCorriente cuentaIngreso, CuentaCorriente cuentaEgreso, double monto) {
		cuentaIngreso.retirarSaldo(monto);
		cuentaEgreso.ingresarSaldo(monto);
	}

}
