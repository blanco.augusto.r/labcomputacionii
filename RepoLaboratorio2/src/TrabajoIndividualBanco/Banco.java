package TrabajoIndividualBanco;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JOptionPane;

public class Banco {

	public static void main(String[] args) {
		
		CuentaCorriente cuenta1 = new CuentaCorriente("Augusto", 5000, 124876);
		CuentaCorriente cuenta2 = new CuentaCorriente("Fernando", 8000, 124877);
		
		Set<CuentaCorriente> listadoDeCuentas = new HashSet<>();
	    listadoDeCuentas.add(cuenta1);
	    listadoDeCuentas.add(cuenta2);
	    
	    TrasferenciasDeCuentas.Transferencias(cuenta1, cuenta2, 1500);
	    
	    JOptionPane.showMessageDialog(null, "Salida: \n\t"+listadoDeCuentas.toString());
	    

        try {
            ObjectOutputStream flujoDeSalida = new ObjectOutputStream(new FileOutputStream(
                    "E:\\RepoLaboratorio2\\labcomputacionii\\RepoLaboratorio2\\src\\TrabajoIndividualBanco\\listadoDeCuentas.dat"));

            flujoDeSalida.writeObject(listadoDeCuentas);
            flujoDeSalida.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {

            ObjectInputStream flujoDeEntrada = new ObjectInputStream(new FileInputStream(
                    "E:\\RepoLaboratorio2\\labcomputacionii\\RepoLaboratorio2\\src\\TrabajoIndividualBanco\\listadoDeCuentas.dat"));

			@SuppressWarnings("unchecked")
			Set<CuentaCorriente> listadoDeCuentasEntrada = (Set<CuentaCorriente>) flujoDeEntrada.readObject();

            System.out.println("\nEntrada: "+ listadoDeCuentasEntrada.toString());

            flujoDeEntrada.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

	    
	}

}
