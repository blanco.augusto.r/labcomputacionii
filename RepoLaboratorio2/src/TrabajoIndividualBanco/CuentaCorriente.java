package TrabajoIndividualBanco;

import java.io.Serializable;

@SuppressWarnings("serial")
class CuentaCorriente implements Serializable {
	

	private String titular;
	private double saldoCC;
	private int numCuentaCorriente;
	
	public CuentaCorriente(String titular, double saldoCC, int numCuentaCorriente) {
		this.titular = titular;
		this.saldoCC = saldoCC;
		this.numCuentaCorriente = numCuentaCorriente;
	}
	
	
	// Metodos set y get Titular
	public String devolverTitular() {
		return titular;
	}
	public void ingresarTitular(String titular) {
		this.titular = titular;
	}
	
	// Metodos set y get SaldoCC
	public double devolverSaldoCC() {
		return saldoCC;
	}
	public void ingresarSaldo(double ingresarSaldo) {
		this.saldoCC += ingresarSaldo;
	}
	public void retirarSaldo(double retirarSaldo) {
		this.saldoCC -= retirarSaldo;
	}
	
	// Metodos set y get Numero de Cuenta corriente
	public int devolverNumCuentaCorriente() {
		return numCuentaCorriente;
	}
	public void ingresarNumCuentaCorriente(int numCuentaCorriente) {
		this.numCuentaCorriente = numCuentaCorriente;
	}

	@Override
	public String toString() {
		return "\nDatos Cuenta Corriente:" +
				"\nTitular: " + devolverTitular() +
				"\nSaldo: " + devolverSaldoCC() +
				"\nN� Cuenta Corriente: " + devolverNumCuentaCorriente();
	}

	
}
