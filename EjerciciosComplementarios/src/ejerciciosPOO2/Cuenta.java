package ejerciciosPOO2;

public class Cuenta {
	
		String titular;
		double cantidad = 0;
		
		public Cuenta(String titular, double cantidad) {
			this.titular = titular;
			this.cantidad = cantidad;
		}

		public String getTitular() {
			return titular;
		}

		public void setTitular(String titular) {
			this.titular = titular;
		}

		public double getCantidad() {
			return cantidad;
		}

		public double ingresarCantidad(double cantidad) {
			if(cantidad > 0) {
				this.cantidad += cantidad;
			}
			return this.cantidad;
		}

		public double retirarCantidad(double cantidad) {
			this.cantidad -= cantidad;
			if(this.cantidad < 0){
				this.cantidad = 0;
			}
			return this.cantidad;
		}
		
		public String toString() {
			return "Cuenta" + 
					"\nTitular= " + titular + 
					"\nCantidad= " + cantidad;
		}
		
		
		

}
