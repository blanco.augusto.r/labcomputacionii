package ejerciciosPOO3;

public class Libro {
	
	int isbn;
	String titulo;
	String autor;
	int numPaginas;
	
	public Libro(int isbn, String titulo, String autor, int numPaginas) {
		this.isbn = isbn;
		this.titulo = titulo;
		this.autor = autor;
		this.numPaginas = numPaginas;
	}

	public int getIsbn() {
		return isbn;
	}

	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getNumPaginas() {
		return numPaginas;
	}

	public void setNumPaginas(int numPaginas) {
		this.numPaginas = numPaginas;
	}

	public String toString() {
		return "El libro con ISBN:" + isbn + " creado por el autor '" + autor + "', con el titulo '" + titulo + "' tiene " + numPaginas + " p�ginas.";
	}

	
	
}
