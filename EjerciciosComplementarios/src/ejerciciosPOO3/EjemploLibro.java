package ejerciciosPOO3;

import javax.swing.JOptionPane;

public class EjemploLibro {

	public static void main(String[] args) {
		
		Libro libro1 = null;
		Libro libro2 = null;
		
		int isbnNum;
		String title;
		String author;
		int pag;
		
		JOptionPane.showMessageDialog(null, "Ingrese los datos pertenecientes a dos libros:");
		
		for( int i=1 ; i<=2 ; i++ ) {
			
			isbnNum = Integer.parseInt(JOptionPane.showInputDialog(null, "Libro N�"+i+
																	"\n ISBN: "));
			title = JOptionPane.showInputDialog(null,  "Libro N�"+i+
																	"\n T�tulo: ");
			author = JOptionPane.showInputDialog(null,  "Libro N�"+i+
																	"\n Autor: ");
			pag = Integer.parseInt(JOptionPane.showInputDialog(null, "Libro N�"+i+
																	"\n N�mero de P�ginas: "));
			
			if( i == 1) {
				libro1 = new Libro(isbnNum,title,author,pag);
			} else {
				libro2 = new Libro(isbnNum,title,author,pag);
			}
		}

		JOptionPane.showMessageDialog(null, "Libro 1: "+libro1.toString()+
											"\nLibro 2: "+libro2.toString());
		
		if (libro1.getNumPaginas() > libro2.getNumPaginas()) {
			
			JOptionPane.showMessageDialog(null, "El libro que tiene mas paginas "+libro1.toString());
		} else {
			JOptionPane.showMessageDialog(null, "El libro que tiene mas paginas "+libro2.toString());
		}
		
		
		
	}

}
