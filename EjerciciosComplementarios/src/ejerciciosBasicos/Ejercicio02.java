package ejerciciosBasicos;

import java.util.Scanner;

import javax.swing.JOptionPane;

import java.util.Arrays;

public class Ejercicio02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner entrada = new Scanner(System.in);
		
		int x1,x2,x3,x4,num1,num2,num3,num4,num5,num6;
		
		System.out.println("Ingrese cuatro numeros: ");
		
		
		x1 = Integer.parseInt(JOptionPane.showInputDialog(null, "Numero 1: "));
		
		System.out.print("Numero 2: ");
		x2 = entrada.nextInt();
		
		System.out.print("Numero 3: ");
		x3 = entrada.nextInt();
		
		System.out.print("Numero 4: ");
		x4 = entrada.nextInt();
		
		num1 = x1 * x2;
		num2 = x1 * x3;
		num3 = x1 * x4;
		num4 = x2 * x3;
		num5 = x2 * x4;
		num6 = x3 * x4;
		
		int numeros[] = {num1,num2,num3,num4,num5,num6};
		Arrays.sort(numeros);
		
		System.out.println("El numero mayor es: "+numeros[5]);
		
	}

}
