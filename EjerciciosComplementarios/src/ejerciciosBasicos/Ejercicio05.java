package ejerciciosBasicos;

import javax.swing.JOptionPane;

public class Ejercicio05 {

	public static void main(String[] args) {
		
		final double tarifaTrabajador = 20;
		double resultado,horas_Trabajadas;
		
		horas_Trabajadas = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese la cantidad de horas trabajadas."));
		
		if(horas_Trabajadas > 40){
			resultado = (horas_Trabajadas * tarifaTrabajador)*(1.5);
		} else {
			resultado = horas_Trabajadas * tarifaTrabajador;
		}
		
		JOptionPane.showMessageDialog(null, "El resultado de la cantidad de horas trabajadas son: "+resultado);

	}

}
