package ejerciciosBasicos;

import java.util.Scanner;

public class Ejercicio01 {
	
	public static void main(String[] args) {
		// Ejercicio 1
		Scanner entrada = new Scanner(System.in);
		
		String nombre;
		String apellido;
		int edad;
		
		System.out.print("Ingrese su nombre: ");
		nombre = entrada.nextLine();
		System.out.println("");
		
		System.out.print("Ingrese su apellido: ");
		apellido = entrada.nextLine();
		System.out.println("");
		
		System.out.print("Ingrese su edad: ");
		edad = entrada.nextInt();
		System.out.println("");
		
		System.out.println("Usted se llama "+nombre+" "+apellido+" y tiene "+edad+" a�os.");
				
		

	}

}
